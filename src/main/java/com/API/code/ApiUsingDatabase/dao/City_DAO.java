package com.API.code.ApiUsingDatabase.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.API.code.ApiUsingDatabase.model.City;
import com.API.code.ApiUsingDatabase.repository.city_Repository;

@Service
public class City_DAO {
	@Autowired
	 city_Repository cityRepository; 
	
	public City Save(City city)
	{
		return cityRepository.save(city);
		
	}
	
	public List<City> findAll(){
		
		return cityRepository.findAll();
	}
	
	public Optional<City> findOne(Long id)
	{
		
		return cityRepository.findById(id);
	}
	
	public void Delete(City city)
	{
		cityRepository.delete(city);
	}
	
	public void DeleteAll()
	{
		cityRepository.deleteAll();
	
	}

}
