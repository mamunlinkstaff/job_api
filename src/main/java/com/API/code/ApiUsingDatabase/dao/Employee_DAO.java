package com.API.code.ApiUsingDatabase.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.API.code.ApiUsingDatabase.model.Employee;
import com.API.code.ApiUsingDatabase.repository.Employee_Repository;

@Service
public class Employee_DAO {
	
	@Autowired
	 Employee_Repository employeeRepository; 
	
	public Employee Save(Employee emp)
	{
		return employeeRepository.save(emp);
		
	}
	
	public List<Employee> findAll(){
		
		return employeeRepository.findAll();
	}
	
	public Optional<Employee> findOne(Long id)
	{
		
		return employeeRepository.findById(id);
	}
	
	public void Delete(Employee emp)
	{
		employeeRepository.delete(emp);
	}
	
	public void DeleteAll()
	{
		employeeRepository.deleteAll();
	
	}
	

}
