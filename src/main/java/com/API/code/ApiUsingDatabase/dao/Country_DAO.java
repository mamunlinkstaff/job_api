package com.API.code.ApiUsingDatabase.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.API.code.ApiUsingDatabase.model.Country;
import com.API.code.ApiUsingDatabase.repository.country_Repository;

@Service
public class Country_DAO {
	
	@Autowired
	 country_Repository countryRepository; 
	
	public Country Save(Country country)
	{
		return countryRepository.save(country);
		
	}
	
	public List<Country> findAll(){
		
		return countryRepository.findAll();
	}
	
	public Optional<Country> findOne(Long id)
	{
		
		return countryRepository.findById(id);
	}
	
	public void Delete(Country country)
	{
		countryRepository.delete(country);
	}
	
	public void DeleteAll()
	{
		countryRepository.deleteAll();
	
	}

}
