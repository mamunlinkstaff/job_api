package com.API.code.ApiUsingDatabase.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.API.code.ApiUsingDatabase.dao.City_DAO;
import com.API.code.ApiUsingDatabase.model.City;
@RestController
@RequestMapping("/company")
public class City_Controller {
	
	@Autowired
	City_DAO cityDao;
	
	
	@PostMapping("/cities")
	public City addcity(@Valid @RequestBody City city)
	{
			
		return cityDao.Save(city);
	}
	
	@GetMapping("/cities")
	public List<City> getAllCity(){
		
		return cityDao.findAll();
	}
	
	@GetMapping("/cities/{id}")
	public ResponseEntity<City> getCityById(@PathVariable(value="id") Long cityid)
	{
		Optional<City>  city = cityDao.findOne(cityid);
		
		if(city.get() == null)
		{
			return ResponseEntity.notFound().build();
		}
	
		 return ResponseEntity.ok().body(city.get());
	}
	
	
	@PutMapping("/cities/{id}")
	public ResponseEntity<City> updateCity(@PathVariable(value="id") Long cityid, @Valid @RequestBody City newcity )
	{
		
		Optional<City> city= cityDao.findOne(cityid);
		if(city.get()==null)
		{
			return ResponseEntity.notFound().build();
		}
		
		city.get().setCountry_ID(newcity.getCountry_ID());
		city.get().setPrefecture_ID(newcity.getPrefecture_ID());
		city.get().setName_en(newcity.getName_en());	
		city.get().setName_jp(newcity.getName_jp());
		city.get().setCreated_At(newcity.getCreated_At());
		city.get().setCreated_By(newcity.getCreated_By());
		city.get().setUpdated_At(newcity.getUpdated_At());	
		city.get().setUpdated_By(newcity.getUpdated_By());
		city.get().setDeleted_At(newcity.getDeleted_At());
		city.get().setDeleted_By(newcity.getDeleted_By());
	
		
		City updateCity= cityDao.Save(city.get());
		return ResponseEntity.ok().body(updateCity); 
		
	}
	
	@DeleteMapping("/cities/{id}")
	public ResponseEntity<City> deleteCity(@PathVariable(value="id") Long cityid)
	{
		Optional<City>  city = cityDao.findOne(cityid);
		
		if(city.get() == null)
		{
			return ResponseEntity.notFound().build();
		}
		cityDao.Delete(city.get());
	
		 return ResponseEntity.ok().body(city.get());
	}
	

}
