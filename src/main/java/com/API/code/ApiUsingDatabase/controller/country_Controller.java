package com.API.code.ApiUsingDatabase.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.API.code.ApiUsingDatabase.dao.Country_DAO;
import com.API.code.ApiUsingDatabase.model.Country;
@RestController
@RequestMapping("/company")
public class country_Controller {
	
	@Autowired
	Country_DAO countryDao;
	
	
	@PostMapping("/countries")
	public Country addcountry(@Valid @RequestBody Country country)
	{
			
		return countryDao.Save(country);
	}
	
	@GetMapping("/countries")
	public List<Country> getAllCountry(){
		
		return countryDao.findAll();
	}
	
	@GetMapping("/countries/{id}")
	public ResponseEntity<Country> getCountryById(@PathVariable(value="id") Long countryid)
	{
		Optional<Country>  country = countryDao.findOne(countryid);
		
		if(country.get() == null)
		{
			return ResponseEntity.notFound().build();
		}
	
		 return ResponseEntity.ok().body(country.get());
	}
	
	
	@PutMapping("/countries/{id}")
	public ResponseEntity<Country> updateCountry(@PathVariable(value="id") Long countryid, @Valid @RequestBody Country newcountry )
	{
		
		Optional<Country> country= countryDao.findOne(countryid);
		if(country.get()==null)
		{
			return ResponseEntity.notFound().build();
		}
		
		country.get().setCountryID(newcountry.getCountryID());	
		country.get().setName_en(newcountry.getName_en());		
		country.get().setName_jp(newcountry.getName_jp());		
		country.get().setCreated_At(newcountry.getCreated_At());		
		country.get().setCreated_By(newcountry.getCreated_By());	
		country.get().setUpdated_At(newcountry.getUpdated_At());	
		country.get().setUpdated_By(newcountry.getUpdated_By());		
		country.get().setDeleted_At(newcountry.getDeleted_At());		
		country.get().setDeleted_By(newcountry.getDeleted_By());
	
		
		Country updateCountry= countryDao.Save(country.get());
		return ResponseEntity.ok().body(updateCountry); 
		
	}
	
	
	
	
	
	
	
	@DeleteMapping("/countries/{id}")
	public ResponseEntity<Country> deleteCountry(@PathVariable(value="id") Long countryid)
	{
		Optional<Country>  country = countryDao.findOne(countryid);
		
		if(country.get() == null)
		{
			return ResponseEntity.notFound().build();
		}
		countryDao.Delete(country.get());
	
		 return ResponseEntity.ok().body(country.get());
	}
	
	

}
