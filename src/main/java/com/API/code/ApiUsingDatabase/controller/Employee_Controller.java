package com.API.code.ApiUsingDatabase.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.API.code.ApiUsingDatabase.model.Employee;
import com.API.code.ApiUsingDatabase.dao.Employee_DAO;

@RestController
@RequestMapping("/company")
public class Employee_Controller {
	
	@Autowired
	Employee_DAO employeeDao;
	
	@PostMapping("/employees")
	public Employee addEmployee(@Valid @RequestBody Employee emp)
	{
			
		return employeeDao.Save(emp);
	}
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployee(){
		
		return employeeDao.findAll();
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable(value="id") Long empid)
	{
		Optional<Employee>  emp = employeeDao.findOne(empid);
		
		if(emp.get() == null)
		{
			return ResponseEntity.notFound().build();
		}
	
		 return ResponseEntity.ok().body(emp.get());
	}
	
	
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value="id") Long empid, @Valid @RequestBody Employee newemp )
	{
		
		Optional<Employee> emp= employeeDao.findOne(empid);
		if(emp.get()==null)
		{
			return ResponseEntity.notFound().build();
		}
		
		emp.get().setName(newemp.getName());
		emp.get().setExpertise(newemp.getExpertise());
		emp.get().setDesignation(newemp.getDesignation());
		Employee updateEmployee= employeeDao.Save(emp.get());
		return ResponseEntity.ok().body(updateEmployee); 
		
	}
	
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Employee> deleteEmployee(@PathVariable(value="id") Long empid)
	{
		Optional<Employee>  emp = employeeDao.findOne(empid);
		
		if(emp.get() == null)
		{
			return ResponseEntity.notFound().build();
		}
		employeeDao.Delete(emp.get());
	
		 return ResponseEntity.ok().body(emp.get());
	}
	
	@DeleteMapping("/employees/")
	public ResponseEntity<List<Employee>> deleteAll()
	{
		List<Employee> emp= employeeDao.findAll();
		if(emp.size()<1)
		{
			return ResponseEntity.notFound().build();
		}
		employeeDao.DeleteAll();
		
		return ResponseEntity.ok().body(emp);
		
	}
	
	
	
	
	
	
	
	

}
