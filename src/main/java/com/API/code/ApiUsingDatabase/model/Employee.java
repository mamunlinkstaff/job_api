package com.API.code.ApiUsingDatabase.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="employee_details2")
@EntityListeners(AuditingEntityListener.class)
public class Employee {
	
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id_;
		
	
	@NotBlank 
	@Column(name = "name", unique = true)
	private String name;
	@NotBlank 	
	@Size(max = 50)
	private String designation;
	@NotBlank 	

	private String expertise;

		

	
	private Date createdAt;
	
	public long getId() {
		return id_;
	}
	public void setId(long id) {
		this.id_ = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getExpertise() {
		return expertise;
	}
	public void setExpertise(String expertise) {
		this.expertise = expertise;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
		
	

}
