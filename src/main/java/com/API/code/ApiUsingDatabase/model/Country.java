package com.API.code.ApiUsingDatabase.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="country")
@EntityListeners(AuditingEntityListener.class)
public class Country {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	 private long ID;
	 private long countryID;
	 private String name_en;
	 private String name_jp;
	 private Date created_At;
	 private String created_By;
	 private Date updated_At;
	 private String updated_By;
	 private Date deleted_At;
	 private String deleted_By;
	 
	 
		public long getID() {
			return ID;
		}

		public void setID(long iD) {
			ID = iD;
		}

		public long getCountryID() {
			return countryID;
		}

		public void setCountryID(long countryID) {
			this.countryID = countryID;
		}

		public String getName_en() {
			return name_en;
		}

		public void setName_en(String name_en) {
			this.name_en = name_en;
		}

		public String getName_jp() {
			return name_jp;
		}

		public void setName_jp(String name_jp) {
			this.name_jp = name_jp;
		}

		public Date getCreated_At() {
			return created_At;
		}

		public void setCreated_At(Date created_At) {
			this.created_At = created_At;
		}

		public String getCreated_By() {
			return created_By;
		}

		public void setCreated_By(String created_By) {
			this.created_By = created_By;
		}

		public Date getUpdated_At() {
			return updated_At;
		}

		public void setUpdated_At(Date updated_At) {
			this.updated_At = updated_At;
		}

		public String getUpdated_By() {
			return updated_By;
		}

		public void setUpdated_By(String updated_By) {
			this.updated_By = updated_By;
		}

		public Date getDeleted_At() {
			return deleted_At;
		}

		public void setDeleted_At(Date deleted_At) {
			this.deleted_At = deleted_At;
		}

		public String getDeleted_By() {
			return deleted_By;
		}

		public void setDeleted_By(String deleted_By) {
			this.deleted_By = deleted_By;
		} 
	 
	
	 

}
