package com.API.code.ApiUsingDatabase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.API.code.ApiUsingDatabase.model.City;

public interface city_Repository extends JpaRepository< City, Long>{

}
